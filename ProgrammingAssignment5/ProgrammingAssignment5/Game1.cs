using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using TeddyMineExplosion;

namespace ProgrammingAssignment5
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Setup screen resolution
        const int WINDOW_WIDTH = 800;
        const int WINDOW_HEIGHT = 600;

        // Mines support
        bool leftClickStarted = false, leftClickReleased = true;
        Texture2D landMineSprite;
        List<Mine> landMines = new List<Mine>();

        // Teddy support
        int spawnDelay;
        int spawnTimer = 0;
        Random rand = new Random();
        Texture2D teddyBearSprite;
        TeddyBear teddyBear;
        List<TeddyBear> teddyBears = new List<TeddyBear>();

        // Exlosions support
        Texture2D explosionSprite;
        Explosion explosion;
        List<Explosion> explosions = new List<Explosion>();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //Assign resolution with game buffer
            graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;
            graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            //set mouse visible
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Initializing sprites
            landMineSprite = Content.Load<Texture2D>("mine");
            teddyBearSprite = Content.Load<Texture2D>("teddybear");
            explosionSprite = Content.Load<Texture2D>("explosion");

            //set the random spawn delay
            spawnDelay = rand.Next(1000, 4000);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // Mines processing on left mouse click
            MouseState mouse = Mouse.GetState();

            if (mouse.LeftButton == ButtonState.Pressed &&
                leftClickReleased)
            {
                leftClickStarted = true;
                leftClickReleased = false;
            }
            else if (mouse.LeftButton == ButtonState.Released)
            {
                leftClickReleased = true;

                // if left click finished, add new mine to list
                if (leftClickStarted)
                {
                    leftClickStarted = false;

                    landMines.Add(new Mine(landMineSprite, mouse.X, mouse.Y));
                }
            }

            // Spawn new teddy's with random delay
            spawnTimer += gameTime.ElapsedGameTime.Milliseconds;

            if (spawnTimer > spawnDelay)
            {
                spawnTimer = 0;
                spawnDelay = rand.Next(1000, 4000);

                teddyBear = new TeddyBear(teddyBearSprite, new Vector2(NextFloat(-0.5f), NextFloat(0.5f)), WINDOW_WIDTH, WINDOW_HEIGHT);
                teddyBears.Add(teddyBear);
            }
            // play explosions after bear colides with mine
            foreach (TeddyBear bear in teddyBears)
            {
                bear.Update(gameTime);

                foreach (Mine mine in landMines)
                {
                    if (bear.CollisionRectangle.Contains(mine.CollisionRectangle.Center.X, mine.CollisionRectangle.Center.Y))
                    {
                        bear.Active = false;
                        mine.Active = false;

                        explosions.Add(new Explosion(explosionSprite, mine.CollisionRectangle.Center.X, mine.CollisionRectangle.Center.Y));
                    }
                }
                
            }
            // update explosions
            foreach (Explosion explosion in explosions)
            {
                explosion.Update(gameTime);
            }

            // remove inactive bears
            if (teddyBears.Count > 0)
            {
                for (int i = teddyBears.Count - 1; i >= 0; i--)
                {
                    if (!teddyBears[i].Active)
                    {
                        teddyBears.RemoveAt(i);
                    }
                }
            }
            // remove inactive mines
            if (landMines.Count > 0)
            {
                for (int i = landMines.Count - 1; i >= 0; i--)
                {
                    if (!landMines[i].Active)
                    {
                        landMines.RemoveAt(i);
                    }
                }
            }
            // remove inactive explosions
            if (explosions.Count > 0)
            {
                for (int i = explosions.Count - 1; i >= 0; i--)
                {
                    if (!explosions[i].Playing)
                    {
                        explosions.RemoveAt(i);
                    }
                }
            }
            
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            
            spriteBatch.Begin();
            // draw mines
            foreach (Mine mine in landMines)
            {
                mine.Draw(spriteBatch);
            }
            // draw bears
            foreach (TeddyBear bear in teddyBears)
            {
                bear.Draw(spriteBatch);
            }
            // draw explosions
            foreach (Explosion explosion in explosions)
            {
                explosion.Draw(spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

        public float NextFloat(float maxValue)
        {
            return (float)rand.NextDouble() * maxValue;
        }
    }
}
